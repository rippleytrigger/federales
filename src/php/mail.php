<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    use PHPMailer\PHPMailer\SMTP;

    //Load Composer's autoloader
    require 'vendor/autoload.php';
    

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        try {
            # Configuring PHPMAILER
            $mail = new PHPMailer(true);
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->Username   = 'rippleytrigger@gmail.com';                     //SMTP username
            $mail->Password   = 'chinatown26334780&';                               //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            # Sender Data
            $name = str_replace(array("\r","\n"),array(" "," ") , strip_tags(trim($_POST["name"])));
            $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
            $phone = trim($_POST["phone"]);
            $message = trim($_POST["message"]);

            # Receiver Data
            $mail_to = 'xfederales@gmail.com';
            // $mail_to = 'rippleytrigger@gmail.com';
            $mail->addEmbeddedImage('federales-info.jpg', 'logo-federales', 'logo', 'base64', 'image/jpeg');

            # Mail Content
            $subject = "Email de Contacto del Formulario de la página web de Los Federales";

            $content = "Nombre: $name <br/>";
            $content .= "Email: $email <br/>";
            $content .= "Teléfono: $phone <br/>";
            $content .= "Mensaje: <br/> $message <br/>";
            $content .= "<img src='cid:logo-federales'/>";
            

            # HTTP Response Code
            $http_response_code = 0;

            if ( empty($name) OR !filter_var($email, FILTER_VALIDATE_EMAIL) OR empty($phone) OR empty($message)) {
                $http_response_code = 406;
                throw new Exception("Por favor llene el formulario");
            }

            # Recipients
            $mail->setFrom($mail_to);
            $mail->addAddress($mail_to);    
            $mail->addReplyTo($email, $name);

            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->CharSet = 'UTF-8';
            $mail->Subject = $subject;
            $mail->Body    = $content;
            $mail->AltBody = $content;

            if($mail->send()){
                # Set a 200 (okay) response code.
                http_response_code(200);
                echo "¡¡¡Gracias!!! Tu mensaje ha sido enviado";
                exit;
            } else {
                $http_response_code = 500;
                throw new Exception("Ha habido un error en relación a tu mensaje. Vuelva a intentarlo.");
            };

            
        } catch(Exception $e) {
            http_response_code($http_response_code);
            echo $e->getMessage();
            exit;
        }
    } else {
        http_response_code(405);
        echo "Hubo un problema con tu envío de correo. Intente nuevamente.";
        exit;
    }

        
?>