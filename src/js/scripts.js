/*!
* Start Bootstrap - Creative v7.0.4 (https://startbootstrap.com/theme/creative)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-creative/blob/master/LICENSE)
*/
//
// Scripts
// 


window.addEventListener('DOMContentLoaded', event => {

    // Navbar shrink function
    var navbarShrink = function () {
        const navbarCollapsible = document.body.querySelector('#mainNav');
        if (!navbarCollapsible) {
            return;
        }
        if (window.scrollY === 0) {
            navbarCollapsible.classList.remove('navbar-shrink')
        } else {
            navbarCollapsible.classList.add('navbar-shrink')
        }

    };

    // Shrink the navbar 
    navbarShrink();

    // Shrink the navbar when page is scrolled
    document.addEventListener('scroll', navbarShrink);

    // Activate Bootstrap scrollspy on the main nav element
    const mainNav = document.body.querySelector('#mainNav');
    if (mainNav) {
        new bootstrap.ScrollSpy(document.body, {
            target: '#mainNav',
            offset: 74,
        });
    };

    // Collapse responsive navbar when toggler is visible
    const navbarToggler = document.body.querySelector('.navbar-toggler');
    const responsiveNavItems = [].slice.call(
        document.querySelectorAll('#navbarResponsive .nav-link')
    );
    responsiveNavItems.map(function (responsiveNavItem) {
        responsiveNavItem.addEventListener('click', () => {
            if (window.getComputedStyle(navbarToggler).display !== 'none') {
                navbarToggler.click();
            }
        });
    });

    // Activate SimpleLightbox plugin for portfolio items
    new SimpleLightbox({
        elements: '#portfolio a.portfolio-box'
    });

    // Contact Form Ajax
    const contactForm = document.body.querySelector('#contactForm');
    const submitSuccessMessage = document.body.querySelector('#submitSuccessMessage div.fw-bolder');
    const submitSuccessDiv = document.body.querySelector('#submitSuccessMessage');
    const submitErrorMessage = document.body.querySelector('#submitErrorMessage div.fw-bolder');
    const submitErrorDiv = document.body.querySelector('#submitErrorMessage');

    contactForm.addEventListener('submit', function(e) {

        e.preventDefault();

        let formData = new FormData(contactForm);

         // create XHR object
        let xml = new XMLHttpRequest();
        // the function with the 3 parameters
        xml.open('POST', contactForm.action, true);
        // the function called when an xhr transaction is completed
         xml.onload  = function(){
            
            if (this.status == 200) {
                submitSuccessDiv.classList.remove('d-none');
                submitSuccessDiv.classList.add('d-block');
                submitErrorDiv.classList.remove('d-block');
                submitErrorDiv.classList.add('d-none');
                submitSuccessMessage.innerHTML=this.responseText;
            } else {
                submitSuccessDiv.classList.add('d-none');
                submitSuccessDiv.classList.remove('d-block');
                submitErrorDiv.classList.add('d-block');
                submitErrorDiv.classList.remove('d-none');
                submitErrorMessage.innerHTML = this.responseText;
            }
        }

        // the function that sends the request
        xml.send(formData);  
    })

    // Loading Animation
    const loader = document.querySelector(".loader");
         window.onload = function(){
           setTimeout(function(){
             loader.style.opacity = "0";
             setTimeout(function(){
               loader.style.display = "none";
             }, 500);
           },1500);
        }
});
